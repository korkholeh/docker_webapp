#!/bin/bash

iscloud=`ps auxw | grep cloud9 | grep -v run_cloud9.sh | grep -v grep`

if [ -z "$iscloud" ]
then
    cd /root/cloud9
    bin/cloud9.sh -l 0.0.0.0 -w /home/app/webapp --username $IDE_USER --password $IDE_PASSWORD &
fi
