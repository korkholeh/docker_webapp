#!/bin/bash

cd /root/cloud9
bin/cloud9.sh -l 0.0.0.0 -w /home/app/webapp --username $IDE_USER --password $IDE_PASSWORD &

cd ~
crontab -l > mycron
echo "* * * * * /root/run_cloud9.sh" >> mycron
crontab mycron
rm -f mycron

cd /home/app
setfacl -R -m d:u:app:rwx,u:app:rwx webapp
setfacl -R -m d:u:www-data:rwx,u:www-data:rwx webapp
setfacl -R -m d:u:root:rwx,u:root:rwx webapp
