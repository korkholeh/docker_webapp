# Task list for future works with container image

## 16.05.2014

* + Add variables `IDE_USER` and `IDE_PASSWORD`
* + Permissions for directory `/home/app/webapp`
* + Solution for error pages
* Support for git in Cloud9
* Rebuild image from scratch
