FROM phusion/passenger-full:0.9.10

ENV HOME /root

CMD ["/sbin/my_init"]

RUN sed -i "/^# deb.*backports/ s/^# //" /etc/apt/sources.list

RUN apt-get -qq update
RUN apt-get -y upgrade
RUN apt-get install -y wget psmisc mc daemontools curl lynx
RUN locale-gen en_US
RUN locale-gen en_US.UTF-8
RUN locale-gen uk_UA
RUN locale-gen uk_UA.UTF-8
RUN locale-gen ru_RU
RUN locale-gen ru_RU.UTF-8
RUN update-locale LC_ALL=uk_UA.UTF-8 LANG=uk_UA.UTF-8

# Install Python 2.7
RUN apt-get -y install python2.7 python-pip python-dev libpq-dev
RUN apt-get -y build-dep python-imaging
RUN apt-get -y install libxml2-dev apache2-utils

RUN alias python="python2.7"
ADD .bash_aliases /home/app/.bash_aliases
ADD .bash_aliases /root/.bash_aliases

RUN pip install django

RUN ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
RUN ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
RUN ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/

RUN pip install pillow
RUN pip install psycopg2
RUN pip install paste

# Enable Nginx and Passenger
RUN rm -f /etc/service/nginx/down
ADD webapp.conf /etc/nginx/sites-enabled/webapp.conf
RUN mkdir /home/app/webapp

ADD passenger_wsgi.py /home/app/webapp/passenger_wsgi.py

# Enable the Redis service.
RUN rm -f /etc/service/redis/down

# Enable the memcached service.
RUN rm -f /etc/service/memcached/down

# Install Cloud9
RUN npm install -g sm
RUN git clone https://github.com/ajaxorg/cloud9.git /root/cloud9
RUN cd /root/cloud9 && sm install
ADD run_cloud9.sh /root/run_cloud9.sh
ENV IDE_USER ideuser
ENV IDE_PASSWORD idepassword

RUN mkdir -p /etc/my_init.d
ADD startup.sh /etc/my_init.d/startup.sh

# Install Goaccess
echo "deb http://deb.goaccess.prosoftcorp.com $(lsb_release -cs) main" | tee -a /etc/apt/sources.list
wget -O - http://deb.goaccess.prosoftcorp.com/gnugpg.key | apt-key add -
apt-get -qq update
apt-get -y install goaccess

VOLUME /var/log
VOLUME /root/.ssh
VOLUME /home/app
VOLUME /etc/nginx/sites-enabled

RUN echo -n uk_UA.UTF-8 > /etc/container_environment/LANG

# Remove authorized_keys for app user
RUN rm -rf /home/app/.ssh/*

EXPOSE 80

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
