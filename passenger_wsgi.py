def application(environ, start_response):
    start_response('200 OK', [('Content-type', 'text/html'), ('X-Foo', 'bar')])
    return ['Hello World!']

# Uncomment the next lines for serving tha Django project

# import os, sys
# sys.path = ['/home/app/webapp', '/home/app/webapp/hello'] + sys.path
# os.environ['DJANGO_SETTINGS_MODULE'] = 'hello.settings'

# import django
# import django.core.handlers.wsgi
# from distutils.version import LooseVersion

# if LooseVersion(django.get_version()) >= LooseVersion('1.7'):
#     django.setup()

# application = django.core.handlers.wsgi.WSGIHandler()
