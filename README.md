# Docker image for web apps

## Error handling does not work under Passenger

We found a solution.

For Django we can use the custom error handler:

    # settings.py
    E500_SHOW_TRACEBACK = True

    # urls.py
    handler500 = 'hello.views.e500'

    # views.py
    from django.conf import settings
    def e500(request):
        import traceback, sys
        user = request.user
        value = ''
        if settings.E500_SHOW_TRACEBACK:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            value = traceback.format_exception(exc_type, exc_value,
                        exc_traceback)

        return render(request, '500.html', {
            'exception_value': value,
            })

    # 500.html
    <html>
    <head>
        <title>500 Server Error</title>
    </head>
    <body>
    <h1>500 Server Error</h1>
    {% if exception_value %}
    <pre>
    {% for e in exception_value %}
    {{ e }}
    {% endfor %}
    </pre>
    {% endif %}
    </body>
    </html>

And never use the `DEBUG = True` because application freezes.
